class Machine < ActiveRecord::Base
  attr_accessible :coca_cole, :sandwich, :snickers, :value, :water, :change

 def add_available_change(amount)
  self.change = self.change + amount
  self.save
 end

  def add_coins(amount)
		self.value = self.value + amount
    self.change = self.change + amount
		self.save
	end

	def add_stock(stock_item)
		if stock_item=="water_restock"
			self.water= self.water + 5
			self.save 
		elsif stock_item=="coca_cole_restock"
			self.coca_cole= self.coca_cole + 5
  		self.save
		elsif stock_item=="sandwich_restock"
			self.sandwich= self.sandwich + 5
  		self.save
		elsif stock_item=="snickers_restock"
			self.snickers= self.snickers + 5
  		self.save
  	end
  end

	def purchase(item,price)
		if item=="water"
			if self.value < price.to_f
        return "Please add more coins."
      elsif self.water < 1
        "The item is out of stock. Please choose another."
     	else
        self.change = self.change + price.to_f - self.value
    		self.value = self.value - price.to_f
    		self.water = self.water - 1
    		@message="You just bought a bottle of Water and your change are #{self.value}"
    		self.value=0;
  	    self.save
  	    return @message
  		end

    elsif item=="snickers"
			if self.value < price.to_f 
        return "Please add more coins."
      elsif self.snickers < 1
        "The item is out of stock. Please choose another."
     	else
        self.change = self.change + price.to_f - self.value
        self.value = self.value - price.to_f
        self.snickers = self.snickers - 1
      	@message="You just bought a Snickers and your change are #{self.value}"
        self.value=0
			  self.save
			  return @message
      end

		elsif item=="coca_cole"
			if self.value < price.to_f
        return "Please add more coins."
      elsif self.coca_cole < 1
      	"The item is out of stock. Please choose another."
      else
        self.change = self.change + price.to_f - self.value
  		  self.value = self.value - price.to_f
  		  self.coca_cole = self.coca_cole - 1
  		  @message="You just bought a coke and your change are #{self.value}"
  		  self.value=0
	      self.save
				return @message
     	end

		elsif item=="sandwich"
			if self.value < price.to_f
        return "Please add more coins."
      elsif self.sandwich < 1
    		"The item is out of stock. Please choose another."
     	else
        self.change = self.change + price.to_f - self.value
    		self.value = self.value - price.to_f
      	self.sandwich = self.sandwich - 1
        @message="You just bought a Sandwich and your change are #{self.value}"
        self.value=0
				self.save
				return @message
     	end
		end
	end
end
