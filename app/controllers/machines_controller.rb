class MachinesController < ApplicationController
  def index
    # at first we are testing if the app runs for the first time.
    @vending = Machine.first
    if @vending.nil?
      @vending = Machine.create(water: 5, snickers: 5 , sandwich: 5 , coca_cole: 5 , value: 0, change: 2)
    end
    @vending = Machine.first
    if params[:amount]!=nil
       @vending.add_coins(params[:amount].to_f)
       redirect_to machines_url

    elsif params[:item]!=nil
      redirect_to machines_url , notice: @vending.purchase(params[:item],params[:price])

    elsif params[:restock]!=nil
      @vending.add_stock(params[:restock])
      redirect_to machines_url

    elsif params[:change]!=nil
      @vending.add_available_change(2)
      redirect_to machines_url , notice: "2 pounds were added to the current change for the Vending machine"
    end
  end
end
