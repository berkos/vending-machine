class AddChangeToMachines < ActiveRecord::Migration
  def change
    add_column :machines, :change, :decimal, precision: 6 , scale: 2
  end
end
