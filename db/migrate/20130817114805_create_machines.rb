class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.decimal :value ,precision: 6 , scale: 2
      t.integer :snickers
      t.integer :coca_cole
      t.integer :sandwich
      t.integer :water

      t.timestamps
    end
  end
end
