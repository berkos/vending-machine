VendingMachine::Application.routes.draw do  
  resources :machines
  root to: 'machines#index', as: 'machines'
end
